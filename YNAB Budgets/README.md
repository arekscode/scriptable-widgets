In case you don’t know YNAB is a budgeting app. Sadly, it does not have a widget but it does has an API you can use. As such, this shortcut will require you to first grab an API key from https://app.youneedabudget.com/settings/developer. Just put it into settings.token on the top of the file. 

To specify the behaviour of the script you need pass it some parameters (long/hard press on widget > Edit Widget > Parameter). The parameters syntax is quite simple: {mode},{comma separated list of category groups to include OR exclamation prefixed categories to ignore}. Notice how you choose which category GROUPS to include and which INDIVIDUAL categories to ignore.

The mode can be ‘double’, ‘wide’, or something else but it must be something. It controls the way the categories are displayed. There shouldn’t be any spaces in between each element. Capitalization matters. Categories to ignore can be prefixed with an exclamation point. 

Lets say you have a category group called “Fixed”, which has quite a lot of categories within in so you want to use the double layout mode but you don’t want your “Water Bill” category to be displayed. You would use the following parameter: double,Fixed,!Water Bill
