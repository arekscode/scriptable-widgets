// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: purple; icon-glyph: dollar-sign;
/* eslint-disable no-undef */
const settings = {
  token: '158b90c3c7578c6e2ba931a1cae676f5d9d6cae53bae0db5baea70a8d5b45b44',
  barHeight: 3,
  barWidth: 100,
  colourScheme: 'dark',
  font: new Font('Halvetics', 12),
  showCatGroup: true,
  roundNums: true,
  warnFactor: 1.2,
  pacerOffset: 1.5,
  cornerWidth: 1,
  cornerHeight: 1,
  wide: false,
};

const uuidRegExp = new RegExp(
  '^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}',
  'g'
);

const colourPallete = {
  green: new Color('2ecc71'),
  orange: new Color('f39c12'),
  red: new Color('e74c3c'),
  white: new Color('ecf0f1'),
  black: new Color('000000'),
  gold: new Color('f1c40f'),
  purple: new Color('9b59b6'),
  blue: new Color('3498db'),
  lgray: new Color('bdc3c7'),
  dgray: new Color('7f8c8d'),
};

const colours = {
  good: colourPallete.green,
  alright: colourPallete.orange,
  bad: colourPallete.red,
  bg: colourPallete.black,
  text: colourPallete.white,
  accent: colourPallete.blue,
  accent2: colourPallete.gold,
  accent3: colourPallete.lgray,
};

async function getBudgets() {
  const budgetsReq = new Request('https://api.youneedabudget.com/v1/budgets');

  budgetsReq.headers = {
    Authorization: `Bearer ${settings.token}`,
  };

  const resp = await budgetsReq.loadJSON();

  const ret = [];

  for (const retBud of resp.data.budgets) {
    ret.push(retBud.id);
    continue;
  }

  if (ret.length === 0) {
    return -1;
  }
  return ret;
}

async function getCats(budgets, toRead, toIgnore) {
  let catGs = [];

  for (const budget of budgets) {
    const catReq = new Request(
      `https://api.youneedabudget.com/v1/budgets/${budget}/categories`
    );
    catReq.headers = {
      Authorization: `Bearer ${settings.token}`,
    };

    const catResp = await catReq.loadJSON();
    if (typeof catResp.data !== 'undefined') {
      catGs = catGs.concat(catResp.data.category_groups);
    }
  }

  const ret = [];
  for (const catG of catGs) {
    if (toRead.includes(catG.name)) {
      for (const cat of catG.categories) {
        if (!toIgnore.includes(cat.name)) {
          ret.push({
            name: cat.name,
            balance: cat.balance,
            budgeted: cat.budgeted,
            activity: Math.abs(cat.activity),
            group: {
              name: catG.name,
              id: catG.id,
            },
            goal: {
              type: cat.goal_type,
              deadline: cat.goal_target_month,
              target: cat.goal_target,
              created: cat.goal_creation_month,
              percDone: cat.goal_percentage_complete,
            },
          });
        }
      }
    }
  }
  return ret;
}
function barBuilder(cat, mode, first, second, pacer) {
  if (typeof first === 'undefined' || typeof cat === 'cat') {
    return -1;
  }
  second = typeof second !== 'undefined' ? second : 'none';
  pacer = typeof pacer !== 'undefined' ? pacer : 'none';

  const drawContext = new DrawContext();
  const barWidth = mode === 'wide' ? settings.barWidth * 2 : settings.barWidth;
  drawContext.size = new Size(barWidth, settings.barHeight);

  drawContext.opaque = false;
  drawContext.respectScreenScale = true;
  drawContext.setTextAlignedCenter();

  const bgPath = new Path();
  bgPath.addRoundedRect(
    new Rect(0, 0, barWidth, settings.barHeight),
    settings.cornerWidth,
    settings.cornerHeight
  );
  drawContext.addPath(bgPath);
  drawContext.setFillColor(colours.text);
  drawContext.fillPath();

  const firstPath = new Path();
  firstPath.addRoundedRect(
    new Rect(0, 0, barWidth * first, settings.barHeight),
    settings.cornerWidth,
    settings.cornerHeight
  );
  drawContext.setFillColor(colours.accent);
  drawContext.addPath(firstPath);
  drawContext.fillPath();

  if (second !== 'none') {
    const secondPath = new Path();
    secondPath.addRoundedRect(
      new Rect(0, 0, barWidth * second, settings.barHeight),
      settings.cornerWidth,
      settings.cornerHeight
    );
    drawContext.setFillColor(colours.good);
    drawContext.addPath(secondPath);
    drawContext.fillPath();
  }

  if (pacer !== 'none') {
    const pacerPath = new Path();
    pacerPath.addRect(
      new Rect(
        barWidth * pacer - settings.pacerOffset,
        0,
        settings.pacerOffset * 2,
        settings.barHeight
      )
    );
    drawContext.setFillColor(colours.bad);
    drawContext.addPath(pacerPath);
    drawContext.fillPath();
  }

  return drawContext.getImage();
}

function makeBar(bar, cat, mode) {
  let img;
  const now = new Date();
  if (
    cat.goal.type === 'TBD' ||
    (cat.goal.type === 'NEED' && cat.goal.deadline !== null)
  ) {
    // Total balance with deadline
    // OR Need for spending with a specific deadline
    const dDate = new Date(cat.goal.deadline);
    const cDate = new Date(cat.goal.created);
    const dDiff = dDate - cDate;

    const pacer = (now - cDate) / dDiff;

    img = barBuilder(
      cat,
      mode,
      cat.goal.percDone / 100,
      cat.activity / cat.goal.target,
      pacer
    );
  } else if (cat.goal.type === 'TB') {
    // Total balance without deadline
    img = barBuilder(cat, mode, cat.goal.percDone / 100);
  } else if (cat.goal.type === 'NEED') {
    // Need for spending without a specific deadline - monthly is assumed
    const pacer =
      now.getDate() /
      new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate();
    img = barBuilder(
      cat,
      mode,
      cat.goal.percDone / 100,
      cat.activity / cat.goal.target,
      pacer
    );
  } else if (cat.goal.type === 'MF') {
    // monthly amount
    const pacer =
      now.getDate() /
      new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate();
    img = barBuilder(cat, mode, cat.goal.percDone / 100, undefined, pacer);
  }

  const barImg = bar.addImage(img);
  barImg.centerAlignImage();
}

function makeTitle(titleStack, cat, mode) {
  titleStack.addSpacer(10);
  titleStack.bottomAlignContent();

  const left = titleStack.addStack();
  left.centerAlignContent();
  const titleLen = mode === 'wide' ? 30 : 10;
  const name = left.addText(
    (function () {
      if (cat.name.length > titleLen) {
        return `${cat.name.substr(0, titleLen)}...`;
      }
      return cat.name;
    })()
  );

  name.font = settings.font;

  if (settings.showCatGroup) {
    left.addSpacer(2);
    const catGroup = left.addText(cat.group.name);
    catGroup.font = new Font('Halvetics', 10);
    catGroup.textColor = colours.accent2;
  }

  titleStack.addSpacer(null);

  const right = titleStack.addStack();

  left.font = settings.font;
  left.textColor = colours.text;

  const spent = right.addText(
    settings.roundNums
      ? Math.ceil(cat.activity / 1000).toString()
      : (cat.activity / 1000).toFixed(2).toString()
  );
  right.addSpacer(2);
  const available = right.addText(
    settings.roundNums
      ? Math.floor(cat.balance / 1000).toString()
      : (cat.balance / 1000).toFixed(2).toString()
  );

  spent.font = settings.font;
  spent.textColor = colours.bad;

  right.addSpacer(2);
  available.font = settings.font;
  titleStack.addSpacer(10);

  available.textColor = colours.good;
}

function makeCat(widget, cat, mode) {
  const container = widget.addStack();
  container.layoutVertically();

  const titleStack = container.addStack();

  makeTitle(titleStack, cat, mode);
  const bar = container.addStack();
  bar.setPadding(0, 10, 0, 10);
  makeBar(bar, cat, mode);
}

async function main() {
  //   let dummy = 'wide,Flex,True,!Car,!MOT';
  const params = args.widgetParameter.split(',');
  Notification.removeAllPending();

  if (settings.colourScheme === 'light') {
    colours.text = colourPallete.black;
    colours.bg = colourPallete.white;
  }

  const budgets = await getBudgets();

  if (budgets === -1) {
    return;
  }

  const mode = params[0];
  params.shift();

  const toRead = [];
  const toIgnore = [];

  for (cat of params) {
    if (cat.charAt(0) === '!') {
      toIgnore.push(cat.substr(1));
    } else {
      toRead.push(cat);
    }
  }

  const catsData = await getCats(budgets, toRead, toIgnore);

  const widget = new ListWidget();
  widget.backgroundColor = colours.bg;
  widget.setPadding(0, 0, 0, 0);
  let left;
  let right;
  if (mode === 'double') {
    console.log('double');
    widget.setPadding(0, 0, 0, 0);

    const split = widget.addStack();
    split.layoutHorizontally();
    left = split.addStack();

    left.layoutVertically();
    left.setPadding(0, 0, 0, 0);

    left.size = new Size(0, 0);
    right = split.addStack();
    right.layoutVertically();
    right.setPadding(0, 0, 0, 0);

    right.size = new Size(0, 0);
  }
  for (const i in catsData) {
    if (mode === 'double') {
      if (i === 0 || i % 2 === 0) {
        makeCat(left, catsData[i]);
        left.addSpacer(5);
      } else {
        makeCat(right, catsData[i]);
        right.addSpacer(5);
      }
      continue;
    }
    makeCat(widget, catsData[i], mode);
    widget.addSpacer(5);
  }
  Script.setWidget(widget);
}

await main();

Script.complete();
